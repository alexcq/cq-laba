import http from './http'

const getAlbums = data => {
  return http({
    url: '/albums',
    method: 'get',
    data
  })
}

const createAlbum = data => {
  return http({
    url: '/albums',
    method: 'post',
    body: JSON.stringify(data),
    headers: {
      'Content-type': 'application/json; charset=UTF-8'
    }
  })
}

export {
  getAlbums,
  createAlbum
}
