import {
  ALBUMS_FAIL,
  ALBUMS_FETCHING,
  ALBUMS_SUCCESS,

  CREATE_ALBUM_FAIL,
  CREATE_ALBUM_SUCCESS
} from '../types'

import { api } from '../../services'

const getAlbums = () => dispatch => {
  dispatch({
    type: ALBUMS_FETCHING,
    payload: true
  })
  return api.albums.getAlbums()
    .then(({ data }) => {
      if (data) {
        dispatch({
          type: ALBUMS_SUCCESS,
          payload: data
        })
      }
      dispatch({
        type: ALBUMS_FETCHING,
        payload: false
      })
    }, (data) => {
      dispatch({
        type: ALBUMS_FAIL,
        payload: data
      })
    })
}

const createAlbum = (data) => dispatch => {
  dispatch({
    type: ALBUMS_FETCHING,
    payload: true
  })
  return api.albums.createAlbum(data)
    .then(({ data }) => {
      if (data) {
        dispatch({
          type: CREATE_ALBUM_SUCCESS,
          payload: data
        })
      }
      dispatch({
        type: ALBUMS_FETCHING,
        payload: false
      })
    }, (data) => {
      dispatch({
        type: CREATE_ALBUM_FAIL,
        payload: data
      })
    })
}

/*
const createCategory = (data) => dispatch => {
  dispatch({
    type: CREATE_CATEGORY_FETCHING,
    payload: true
  })
  return api.categories.createCategory(data)
    .then(({ data }) => {
      if (data) {
        dispatch({
          type: CREATE_CATEGORY_SUCCESS,
          payload: data
        })
      }
      dispatch({
        type: CREATE_CATEGORY_FETCHING,
        payload: false
      })
    })
}

const removeCategoryById = (id) => dispatch => {
  dispatch({
    type: REMOVE_CATEGORY_BY_ID_FETCHING,
    payload: true
  })
  return api.categories.removeCategoryById(id)
    .then(({ data }) => {
      if (data) {
        dispatch({
          type: REMOVE_CATEGORY_BY_ID_SUCCESS,
          payload: id
        })
      }
      dispatch({
        type: REMOVE_CATEGORY_BY_ID_FETCHING,
        payload: false
      })
    })
}

const updateCategoryById = (id, data) => dispatch => {
  dispatch({
    type: UPDATE_CATEGORY_BY_ID_FETCHING,
    payload: true
  })
  return api.categories.updateCategoryById(id, data)
    .then(({ data }) => {
      if (data) {
        dispatch({
          type: UPDATE_CATEGORY_BY_ID_SUCCESS,
          payload: data
        })
      }
      dispatch({
        type: UPDATE_CATEGORY_BY_ID_FETCHING,
        payload: false
      })
    })
}
*/

export {
  getAlbums,
  createAlbum
}
