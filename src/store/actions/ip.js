import {
  IP_ADD,
  // IP_DELETE,
  IP_LIST,
  IP_UPDATE
} from '../types'

const getIpList = () => dispatch => {
  dispatch({
    type: IP_LIST,
    payload: true
  })
}

const addIp = (data) => dispatch => {
  dispatch({
    type: IP_ADD,
    payload: data
  })
}

const updateIp = (data) => dispatch => {
  dispatch({
    type: IP_UPDATE,
    payload: data
  })
}

export {
  getIpList,
  addIp,
  updateIp
}
