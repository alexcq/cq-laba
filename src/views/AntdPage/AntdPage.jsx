import React, { Component } from 'react'
import { Table, Icon, Typography, Button, Modal, Input } from 'antd'

const { Title } = Typography

class AntdPage extends Component {
  state = {
    visible: false,
    ip: '',
    label: '',
    isEdit: false
  }

  handleCancel = () => {
    this.setState({
      ip: '',
      label: '',
      visible: false,
      isEdit: false
    })
  }

  handleVisibleModal = visible => {
    this.setState({
      visible
    })
  }

  handleInput = (key, value) => {
    this.setState({
      [key]: value
    })
  }

  handleSubmit = () => {
    const { ip, label, isEdit } = this.state

    const data = { ip, label }

    if (isEdit) {
      this.props.updateIp(data)
    } else {
      this.props.addIp(data)
    }

    this.handleCancel()
  }

  handleEdit = ({ ip, label }) => {
    this.setState({
      ip,
      label,
      visible: true,
      isEdit: true
    })
  }

  render () {
    const {
      ips: { list }
    } = this.props

    const { visible, ip, label, isEdit } = this.state

    const columns = [
      { title: 'Ip', dataIndex: 'ip', key: 'ip' },
      { title: 'Label', dataIndex: 'label', key: 'label' },
      {
        title: 'Action',
        dataIndex: '',
        key: 'x',
        render: item => <Button onClick={() => this.handleEdit(item)}>
          <Icon type='edit' />Edit
        </Button>
      }
    ]

    return <div style={{ maxWidth: '1200px', margin: '0 auto' }}>
      <Modal
        title={isEdit ? `Edit ${label}` : 'Create Ip'}
        visible={visible}
        onOk={this.handleSubmit}
        onCancel={this.handleCancel}
      >

        <div style={{ marginBottom: 16 }}>
          <Input
            addonBefore='Ip'
            value={ip}
            onChange={e => this.handleInput('ip', e.target.value)}
            disabled={isEdit}
          />
        </div>
        <div style={{ marginBottom: 16 }}>
          <Input addonBefore='Label' value={label} onChange={e => this.handleInput('label', e.target.value)} />
        </div>
      </Modal>

      <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
        <Title level={2}>Ips</Title>
        <Button onClick={() => this.handleVisibleModal(true)}>
          <Icon type='plus-circle' />Add
        </Button>
      </div>

      <Table
        columns={columns}
        dataSource={list}
      />
    </div>
  }
}

export default AntdPage
