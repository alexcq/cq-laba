import React, { Fragment } from 'react'
import { Button } from '../../components'
import { Welcome } from './styled'
import blackBoxImage from './media/blackbox.png'
import history from '../../history'

const btnStyle = {
  maxWidth: 300,
  with: 300
}

const IndexPage = () => {
  return (
    <Fragment>
      <Welcome>
        <Welcome.Card>
          <img src={blackBoxImage} alt='' />
          <Welcome.CardTitle>Welcome to index page</Welcome.CardTitle>
          <Button
            type='primary'
            style={btnStyle}
            onClick={() => history.push('/main')}
          >
            Class: Basic. Routing
          </Button>
          <br />
          <Button
            type='primary'
            style={btnStyle}
            onClick={() => history.push('/redux')}
          >
            Class: Redux
          </Button>
          <br />
          <Button
            type='primary'
            style={btnStyle}
            onClick={() => history.push('/antd')}
          >
            Antd
          </Button>
          <br />
          <Button
            type='primary'
            style={btnStyle}
            onClick={() => history.push('/rest')}
          >
            Rest
          </Button>
          <br />
          <Button
            type='primary'
            style={btnStyle}
            onClick={() => history.push('/form')}
          >
            Form
          </Button>
        </Welcome.Card>
      </Welcome>
    </Fragment>
  )
}

export default IndexPage
