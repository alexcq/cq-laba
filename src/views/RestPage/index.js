import { connect } from 'react-redux'
import RestPage from './RestPage'
import * as albums from '../../store/actions/albums'

const mapStateToProps = state => ({
  albums: state.albums
})

const mapDispatchToProps = dispatch => ({
  getAlbums: (data) => dispatch(albums.getAlbums()),
  createAlbum: (data) => dispatch(albums.createAlbum(data))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RestPage)
