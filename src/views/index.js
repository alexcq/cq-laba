import IndexPage from './IndexPage'
import MainPage from './MainPage'
import ReduxPage from './ReduxPage'
import AntdPage from './AntdPage'
import RestPage from './RestPage'
import FormPage from './FormPage'

export {
  IndexPage,
  MainPage,
  ReduxPage,
  AntdPage,
  RestPage,
  FormPage
}
