import React, {Component} from 'react'
import { Form, Input, Button } from 'antd'

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 8 }
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 8 }
  }
}

class FormPage extends Component {
  state = {
    value: '',
    status: '',
    help: ''
  }

  validate = value => {
    if (value > 100) {
      return 'error'
    } else if (value > 50 && value <= 100) {
      return 'warning'
    }
    return 'success'
  }

  handleInput = value => {
    this.setState({
      value,
      status: 'validating'
    })

    setTimeout(() => {
      this.setState({
        status: this.validate(value)
      })
    }, 2000)

  }

  handleSubmit = e => {
    e.preventDefault()

    this.props.form.validateFields(['email', 'username'], (err, values) => {
      if (!err) {
        console.log(values);

      }
    })
  }

  render() {
    const { value, status, help } = this.state

    const { form: { getFieldDecorator } } = this.props

    return <Form {...formItemLayout} onSubmit={this.handleSubmit}>
      <Form.Item label="E-mail">
        {getFieldDecorator('email', {
          rules: [
            {
              type: 'email',
              message: 'The input is not valid E-mail!',
            },
            {
              required: true,
              message: 'Please input your E-mail!',
            },
          ]
        })(<Input/>)}
      </Form.Item>

      <Form.Item label="Username">
        {getFieldDecorator('username', {
          rules: [
            {
              required: true,
              message: 'Please input your username!',
            },
            {
              min: 3,
              message: 'min 3 letters'
            }
          ]
        })(<Input/>)}
      </Form.Item>

      <Form.Item label="Password">
        {getFieldDecorator('password', {
          rules: [
            {
              required: true,
              message: 'Please input your password!',
            }
          ]
        })(<Input.Password />)}
      </Form.Item>

      <Form.Item>
        <Button htmlType='submit'>Submit</Button>
      </Form.Item>

      {false && <Form.Item
        label='Input'
        hasFeedback
        validateStatus={status}
        help={help}
      >
        <Input placeholder='Enter login' id='input' value={value} onChange={e => this.handleInput(e.target.value)}/>
      </Form.Item>
      }
    </Form>
  }
}

export default Form.create({ name: 'test' })(FormPage)
